<?php
/**
 * Created by PhpStorm.
 * User: virga
 * Date: 2018-07-28
 * Time: 15:44
 */

namespace App\EventListener;


use App\Entity\Registration;
use App\Events;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class RegistrationConfirmationNotificationSubscriber implements EventSubscriberInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var string
     */
    private $sender;
    private $twigEngine;

    private  $transport;

    /**
     * @return Swift_Mailer
     */
    public function getMailer(): Swift_Mailer
    {
        return $this->mailer;
    }



    /**
     * Constructor.
     *
     * @param \Swift_Mailer         $mailer
     * @param string                $sender
     * @param \Swift_SmtpTransport  $transport
     */
    public function __construct(\Swift_Mailer $mailer, TwigEngine $twigEngine)
    {
        $this->mailer = $mailer;
        $this->twigEngine=$twigEngine;
    }
    public static function getSubscribedEvents()
    {
        return [
            Events::REGISTRATION_CONFIRMED => 'onRegistrationConfirmed',
        ];
    }
    /**
     * @param GenericEvent $event
     * @param Swift_Mailer $mailer
     */
    public function onRegistrationConfirmed(GenericEvent $event)
    {
        $email = $event->getSubject()[0];
        $username = $event->getSubject()[1];
        $show = $event->getSubject()[2];
        $dog = $event->getSubject()[3];

        /* * @var Registration $registration */
//        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
//            ->setUsername('virginija.prasmickaite@gmail.com')
//            ->setPassword('TEL441344');
//
//
//        $this->mailer = new Swift_Mailer($transport);

        $message = (new Swift_Message())
            ->setSubject('Patvirtinimas / Registration confirmation')
            ->setFrom('no_reply@lambk.com')
            ->setTo($email)
            ->setBody(
                $this->twigEngine->render(
                // templates/emails/registration.html.twig
                    'confirmation.twig',
                    ['user' => $username,
                      'show' => $show,
                        'dog'=>$dog
                        ]
                )
            )
            ->setContentType("text/html");
//        $this->getMailer()->send($message);
        $this->mailer->send($message);

        // In app/config/config_dev.yml the 'disable_delivery' option is set to 'true'.
        // That's why in the development environment you won't actually receive any email.
        // However, you can inspect the contents of those unsent emails using the debug toolbar.
        // See https://symfony.com/doc/current/email/dev_environment.html#viewing-from-the-web-debug-toolbar

    }

}
