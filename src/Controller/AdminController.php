<?php

namespace App\Controller;

use App\Entity\Dog;
use App\Entity\Home;
use App\Entity\Registration;
use App\Events;
use App\Form\HomeType;
use App\Repository\OwnerRepository;
use App\Repository\RegistrationRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;


class AdminController extends BaseAdminController
{
    public function createNewUserEntity()
    {
        return $this->get('fos_user.user_manager')->createUser();
    }

    public function persistUserEntity($user)
    {
        $this->get('fos_user.user_manager')->updateUser($user, false);
        parent::persistEntity($user);
    }

    public function updateUserEntity($user)
    {
        $this->get('fos_user.user_manager')->updateUser($user, false);
        parent::updateEntity($user);
    }

    /**
     * @Route("/admin/registrations", name="pending_registrations_index")
     */
    public function comments(RegistrationRepository $registrationRepository)
    {
        return $this->render('admin/registrations.html.twig', ['registrations' => $registrationRepository->findAllWhereNotConfirmed()]);
    }

    /**
     * @Route("/admin/registration/{id}/confirm", name="registration_confirm")
     * @throws \Doctrine\ORM\ORMException
     */
    public function confirm(Registration $registration, RegistrationRepository $registrationRepository, EventDispatcherInterface $eventDispatcher, Request $request, OwnerRepository $ownerRepository)
    {
        $registrationRepository->setAsIsConfirmed($registration->getId());
        $email = $registration->getDog()->getOwner()->getUser()->getEmail();
        $show = $registration->getShow()->getPlaceAndTime();
        $username = $registration->getDog()->getOwner()->getUser()->getUsername();
        $dog = $registration->getDog()->getRegisteredName();

        $event = new GenericEvent([$email, $username, $show, $dog]);

        $eventDispatcher->dispatch(Events::REGISTRATION_CONFIRMED, $event);

        $this->addFlash('success', "Registration is confirmed / Registracija į parodą patvirtinta");

        return $this->redirectToRoute('pending_registrations_index');
    }

    /**
     * @Route("/", name="admin")
     */
    public function indexAction(Request $request)
    {
        return parent::indexAction($request);
    }

    public function catalogueAction(Request $request)
    {

        $repository = $this->getDoctrine()->getRepository('App:Show::class');

        $id = $request->query->get('id');
        $entity = $repository->find($id);


        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'id' => $id,
            'entity' => $entity,
        ));
    }

}
