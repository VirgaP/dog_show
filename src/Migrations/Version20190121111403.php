<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190121111403 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE title DROP FOREIGN KEY FK_2B36786B634DFEB');
        $this->addSql('ALTER TABLE title ADD CONSTRAINT FK_2B36786B634DFEB FOREIGN KEY (dog_id) REFERENCES dogs (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE title DROP FOREIGN KEY FK_2B36786B634DFEB');
        $this->addSql('ALTER TABLE title ADD CONSTRAINT FK_2B36786B634DFEB FOREIGN KEY (dog_id) REFERENCES dogs (id)');
    }
}
