<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190121134407 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registrations DROP FOREIGN KEY FK_53DE51E7D0C1FC64');
        $this->addSql('ALTER TABLE registrations DROP FOREIGN KEY FK_53DE51E7D364EAFF');
        $this->addSql('ALTER TABLE registrations ADD CONSTRAINT FK_53DE51E7D0C1FC64 FOREIGN KEY (show_id) REFERENCES `show` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE registrations ADD CONSTRAINT FK_53DE51E7D364EAFF FOREIGN KEY (show_class_id) REFERENCES show_class (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registrations DROP FOREIGN KEY FK_53DE51E7D0C1FC64');
        $this->addSql('ALTER TABLE registrations DROP FOREIGN KEY FK_53DE51E7D364EAFF');
        $this->addSql('ALTER TABLE registrations ADD CONSTRAINT FK_53DE51E7D0C1FC64 FOREIGN KEY (show_id) REFERENCES `show` (id)');
        $this->addSql('ALTER TABLE registrations ADD CONSTRAINT FK_53DE51E7D364EAFF FOREIGN KEY (show_class_id) REFERENCES show_class (id)');
    }
}
